﻿using UnityEngine;
using System;
using System.Collections.Generic;       //Allows us to use Lists.
using Random = UnityEngine.Random;      //Tells Random to use the Unity Engine random number generator.

public class BoardManager : MonoBehaviour
{


	public int columns = 8;                                         //Number of columns in our game board.
	public int rows = 8;                                            //Number of rows in our game board.
	public int nrAnimators;
	public GameObject[] backgroundTiles;                            //Array of background prefabs.


	private GameObject tile;
	private Transform boardHolder;                                  //A variable to store a reference to the transform of our Board object.

	private List<GameObject> animatorTiles = new List<GameObject>();
	private GameObject[,] animatedTiles;
	private GameObject backgroundArchetype;

	// Initializes the animators responsible for generating animation phases
	void InitAnimators() {

		// If number of animators is unset, then choose 10% of all tiles
		if (nrAnimators <= 0) {
			nrAnimators = columns * rows / 10;
		}

		// Maximise the number of animators in 256
		nrAnimators = Math.Min(nrAnimators, 256);

		for (int i = 0; i < nrAnimators; i++) {
			//Choose a random animator
			GameObject animatorTile = backgroundTiles[Random.Range (0,backgroundTiles.Length)];
			animatorTile = Instantiate (animatorTile, new Vector3 (-1, -1, 0f), Quaternion.identity) as GameObject;

			// Hide the animator from the screen - it needs to run to generate the sprites
			animatorTile.transform.localScale = new Vector3(0, 0, 0);
			animatorTile.transform.SetParent (boardHolder);
			animatorTiles.Add (animatorTile);
		}

	}

	// Creates a background tile at a specific coordinate and linking it to a randomly chosen
	// animator
	GameObject CreateBackgroundTile(int x, int y) {		
		GameObject backgroundTile =
			Instantiate (backgroundArchetype, new Vector3 (x, y, 0f), Quaternion.identity) as GameObject;	

		// Add the BacgkgroundTile behavior to the backgroundTile GameObject
		BackgroundTile tileScript = backgroundTile.AddComponent<BackgroundTile> ();
		tileScript.SetAnimator(animatorTiles[Random.Range (0, animatorTiles.Count)]);

		backgroundTile.transform.SetParent (boardHolder);
		return backgroundTile;
	}
		
	void BoardSetup ()
	{		
		boardHolder = new GameObject ("Board").transform;

		InitAnimators ();
		animatedTiles = new GameObject[rows, columns];

		backgroundArchetype = new GameObject();
		backgroundArchetype.AddComponent<SpriteRenderer>();

		for(int x = 0; x < columns; x++) {			
			for(int y = 0; y < rows; y++) {				
				animatedTiles[x,y] = CreateBackgroundTile(x, y);
			}
		}
	}		
		
	public void SetupScene ()
	{
		BoardSetup ();
	}

	public void Update() {
	}

}
