﻿using UnityEngine;
using Random = UnityEngine.Random;
using System.Collections;

public class AnimatorTile : MonoBehaviour {

	private Animator animator;
	private string state;
	private bool isWait;

	public string firstState;

	// Use this for initialization
	void Start () {
		state = firstState;
		isWait = false;
		animator = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {

		if (isWait) {
			return;
		}

		if (animator.GetCurrentAnimatorStateInfo (0).normalizedTime > 1 && !animator.IsInTransition (0)) {
			isWait = true;
			Invoke("NextStep", Random.Range(0.5f, 1.5f));
		}	
	}

	void NextStep() {
		isWait = false;

		if (state == "tile0out" || state == "tile1out") {
			if (Random.Range (0, 100) >= (state == "tile0out" ? 30 : 70)) {
				state = "tile1in";
			} else {
				state = "tile0in";
			}
		} else if (state == "tile1in" || state == "tile1") {
			state = "tile1out";
		} else {
			state = "tile0out";
		}

		animator.SetTrigger (state);
	}
}
