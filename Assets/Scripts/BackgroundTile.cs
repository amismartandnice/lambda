﻿using UnityEngine;
using System.Collections;

public class BackgroundTile : MonoBehaviour {

	//public GameObject animator;

	private SpriteRenderer spriteRenderer;
	private SpriteRenderer animatorRenderer;

	private bool isVisible = false;

	void Start () {
		spriteRenderer = GetComponent<SpriteRenderer> ();
		//this.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
	}

	public void SetAnimator(GameObject animator) {
		animatorRenderer = animator.GetComponent<SpriteRenderer> ();
	}

	void OnBecameInvisible() {
		isVisible = false;
	}

	void OnBecameVisible() {
		isVisible = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (!isVisible) {
			return;
		}
		// Copy the generated sprite from the associated animator
		spriteRenderer.sprite = animatorRenderer.sprite;
	}

}
